/* global require */
/* global __dirname */
var gulp    = require('gulp');
var uglify  = require('gulp-uglify');
var ngtpl   = require('gulp-angular-templatecache');
var concat  = require('gulp-concat');
var order   = require('gulp-order');
var connect = require('gulp-connect');

gulp.task('app-tpl', function () {
    return gulp.src(__dirname + '/src/templates/**/*.html')
        .pipe(ngtpl({
            module: 'contentapp.templates',
            root: 'app',
            standalone: true
        }))
        .pipe(gulp.dest(__dirname + '/src/js'));
});
gulp.task('app-css', function () {
    return gulp.src(__dirname + '/src/css/**/*.css')
        .pipe(order())
        .pipe(concat('app.css'))
        .pipe(gulp.dest(__dirname + '/public/assets'));
});
gulp.task('app-js', ['app-tpl'], function () {
    return gulp.src(__dirname + '/src/js/**/*.js')
        .pipe(order([
            'config.js',
            'app.js',
            'services/**/*.js',
            'directives/**/*.js',
            'controllers/**/*.js',
            '**/*.js',
            'templates.js'
        ]))
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest(__dirname + '/public/assets'));
});

gulp.task('component-tpl', function () {
    return gulp.src(__dirname + '/component/templates/**/*.html')
        .pipe(ngtpl({
            module: 'contentapp.component.templates',
            root: 'component',
            standalone: true
        }))
        .pipe(gulp.dest(__dirname + '/component/js'));
});
gulp.task('component-css', function () {
    return gulp.src(__dirname + '/component/css/**/*.css')
        .pipe(order())
        .pipe(concat('component.css'))
        .pipe(gulp.dest(__dirname + '/public/assets'));
});
gulp.task('component-js', ['component-tpl'], function () {
    return gulp.src(__dirname + '/component/js/**/*.js')
        .pipe(order([
            'config.js',
            'module.js',
            'services/**/*.js',
            'directives/**/*.js',
            'controllers/**/*.js',
            '**/*.js',
            'templates.js'
        ]))
        .pipe(concat('component.js'))
        .pipe(uglify())
        .pipe(gulp.dest(__dirname + '/public/assets'));
});

gulp.task('vendors-fonts', function () {
    return gulp.src([
        'bower_components/bootstrap/dist/fonts/**',
        'bower_components/components-font-awesome/fonts/**'
    ])
        .pipe(order())
        .pipe(gulp.dest(__dirname + '/public/assets/fonts/'));
});
gulp.task('vendors-js', function () {
    return gulp.src([
        'bower_components/lodash/dist/lodash.min.js',
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/angular/angular.min.js',
        'bower_components/angular-route/angular-route.min.js',
        'bower_components/angular-ui-router/release/angular-ui-router.min.js',
        'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js'
    ])
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest(__dirname + '/public/assets/'));
});
gulp.task('vendors-css', function () {
    return gulp.src([
        'bower_components/angular/angular-csp.css',
        'bower_components/bootstrap/dist/css/bootstrap.min.css'
    ])
        .pipe(order())
        .pipe(concat('vendors.css'))
        .pipe(gulp.dest(__dirname + '/public/assets/'));
});


gulp.task('watch', function () {
    gulp.watch(__dirname + '/src/templates/**/*.html', [
        'app-tpl'
    ]);
    gulp.watch(__dirname + '/src/js/**/*.js', [
        'app-js'
    ]);
    gulp.watch(__dirname + '/src/css/**/*.css', [
        'app-css'
    ]);

    gulp.watch(__dirname + '/component/templates/**/*.html', [
        'component-tpl'
    ]);
    gulp.watch(__dirname + '/component/js/**/*.js', [
        'component-js'
    ]);
    gulp.watch(__dirname + '/component/css/**/*.css', [
        'component-css'
    ]);
});
gulp.task('server', ['build', 'watch'], function () {
    connect.server({
        root: 'public',
        port: 9000
    });
});


gulp.task('vendors', ['vendors-js', 'vendors-css', 'vendors-fonts']);
gulp.task('component', ['component-js', 'component-css']);
gulp.task('app', ['app-js', 'app-css']);

gulp.task('build', ['vendors', 'component', 'app']);
gulp.task('default', ['build']);