(function() {
    'use strict';

    angular.module('contentapp.config.url', [
        'ui.router'
    ])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider.state('home', {
                url: '/',
                templateUrl: 'app/index.html',
                controller: 'indexController'
            });
            $stateProvider.state('about', {
                url: '/about',
                templateUrl: 'app/about.html',
                controller: 'aboutController'
            });
            $stateProvider.state('component', {
                url: '/component',
                abstract: true,
                templateUrl: 'app/component.html',
                controller: 'componentController'
            });
            $stateProvider.state('contact', {
                url: '/contact',
                templateUrl: 'app/contact.html',
                controller: 'contactController'
            });
        }])
        .config(['$urlRouterProvider', function ($urlRouterProvider) {
            $urlRouterProvider.when('/component', '/component/country/list');
            $urlRouterProvider.when('/component/country', '/component/country/list');
        }]);
})();