(function() {
    'use strict';

    angular.module('contentapp', [
        'ui.router',
        'contentapp.config',
        'contentapp.templates',
        'contentapp.controllers',
        'contentapp.component'
    ])
        .run(['$rootScope', '$state', function ($rootScope, $state) {
            $rootScope.$state = $state;
        }]);
})();