(function() {
    'use strict';

    angular.module('contentapp.controllers', [
        'contentapp.controllers.index',
        'contentapp.controllers.about',
        'contentapp.controllers.contact',
        'contentapp.controllers.component'
    ]);
})();