(function() {
    'use strict';

    angular.module('contentapp.component', [
        'contentapp.component.config',
        'contentapp.component.templates',
        'contentapp.component.controllers',
        'contentapp.component.directives',
        'ui.router'
    ]);
})();