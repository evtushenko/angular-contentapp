(function () {
    'use strict';

    angular.module('contentapp.component.services.country', [])
        .factory('caCountryService', ['$q', '$http', function ($q, $http) {
            var all;

            var factory = {
                list: function () {
                    var d = $q.defer();
                    if (all) {
                        d.resolve(all);
                    } else {
                        $http.get('http://restcountries.eu/rest/v1/all')
                            .success(function (list) {
                                all = list;
                                d.resolve(list);
                            })
                            .error(function (e) {
                                d.reject(e);
                            });
                    }

                    return d.promise;
                },
                item: function (code) {
                    var d = $q.defer();
                    factory.list().then(function (list) {
                        var item = _.find(list, {alpha3Code: code});
                        if (item) {
                            d.resolve(item);
                        } else {
                            d.reject('not found');
                        }
                    }, function (e) {
                        d.reject(e);
                    });

                    return d.promise;
                }
            };
            return factory;
        }]);
})();