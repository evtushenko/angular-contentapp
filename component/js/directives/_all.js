(function() {
    'use strict';

    angular.module('contentapp.component.directives', [
        'contentapp.component.directives.countryview'
    ]);
})();