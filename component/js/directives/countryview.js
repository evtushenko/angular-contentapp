(function () {
    'use strict';

    angular.module('contentapp.component.directives.countryview', [])
        .directive('caCountryView', function () {
            return {
                replace: true,
                templateUrl: 'component/directives/countryview.html',
                scope: {
                    viewAll: '@',
                    country: '='
                }
            };
        });
})();