(function() {
    'use strict';

    angular.module('contentapp.component.config.url', [
        'ui.router'
    ])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
//                .state('component', {
//                    url: '/component',
//                    abstract: true,
//                    views: {
//                        conponent: {
//                            templateUrl: 'component/layout.html'
//                        }
//                    }
//                })
                .state('component.root', {
                    url: '',
                    abstract: true,
                    templateUrl: 'component/layout.html'
                })
                .state('component.root.about', {
                    url: '/about',
                    views: {
                        menu: {
                            templateUrl: 'component/menu.html',
                            controller: 'caMenuController'
                        },
                        content: {
                            templateUrl: 'component/about.html'
                        }
                    }
                })
                .state('component.root.country', {
                    url: '/country',
                    abstract: true,
                    views: {
                        menu: {
                            templateUrl: 'component/menu.html',
                            controller: 'caMenuController'
                        },
                        content: {
                            template: '<div ui-view></div>'
                        }
                    }
                })
                .state('component.root.country.list', {
                    url: '/list',
                    templateUrl: 'component/country/list.html',
                    controller: 'caCountryListController'
                })
                .state('component.root.country.view', {
                    url: '/view/:id',
                    templateUrl: 'component/country/view.html',
                    controller: 'caCountryViewController'
                });
        }]);
})();