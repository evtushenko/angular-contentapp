(function () {
    'use strict';

    angular.module('contentapp.component.controllers.country.view', [
        'contentapp.component.services.country',
        'contentapp.component.directives.countryview'
    ])
        .controller('caCountryViewController', [
            '$scope', '$stateParams', 'caCountryService',
            function ($scope, $stateParams, countryService) {
                console.log('contentapp.component.controllers.country.view');

                $scope.country = {};
                countryService.item($stateParams.id).then(function (item) {
                    $scope.country = item;
                });
        }]);
})();