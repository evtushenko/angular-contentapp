(function () {
    'use strict';

    angular.module('contentapp.component.controllers.country.list', [
        'contentapp.component.services.country'
    ])
        .controller('caCountryListController', [
            '$scope', 'caCountryService',
            function ($scope, countryService) {
                console.log('contentapp.component.controllers.country.list');

                $scope.countries = [];
                countryService.list().then(function (list) {
                    $scope.countries = list;
                });
        }]);
})();