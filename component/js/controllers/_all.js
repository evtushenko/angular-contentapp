(function() {
    'use strict';

    angular.module('contentapp.component.controllers', [
        'contentapp.component.controllers.menu',
        'contentapp.component.controllers.country.list',
        'contentapp.component.controllers.country.view'
    ]);
})();