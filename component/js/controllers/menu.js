(function () {
    'use strict';

    angular.module('contentapp.component.controllers.menu', [
        'contentapp.component.services.country'
    ])
        .controller('caMenuController', [
            '$scope', '$state',
            function ($scope, $state) {
                $scope.$state = $state;
            }]);
})();